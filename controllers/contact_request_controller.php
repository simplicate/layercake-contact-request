<? App::import( 'Controller', 'LayerCake.LayerCakeApp' ); ?>
<? class ContactRequestController extends LayerCakeAppController {

	var $name       = 'ContactRequest';
    var $helpers    = array( 'Html', 'Form', 'Session', 'LayerCake.Cycle', 'Xml' );
    var $uses       = array( 'ContactRequest.ContactRequest' );
    var $components = array( 'Session', 'Auth', 'Email', 'LayerCake.PersistentValidation' );

    function beforeFilter() {
		parent::beforeFilter( );
		$this->Auth->allow( 'submit', 'index', 'confirm' );
	}

    function submit() {
        $this->index();
    }

    function index() {

        // if data was submitted and it validates
        if( !empty( $this->data ) ) {

            $this->ContactRequest->set( $this->data );
            if( $this->ContactRequest->validates() ) {

                // save to the database
                $this->ContactRequest->save();

                $this->data['ContactRequest']['name'] = $this->data['ContactRequest']['first_name'] . ' ' . $this->data['ContactRequest']['last_name'];

                // send the email
                $this->Email->to = Configure::read('ContactRequest.toEmail');
                $this->Email->from = 'website@' . $_SERVER['HOST_NAME'];
                $this->Email->subject = "[Website] Contact Request";
                $this->Email->replyTo = $this->data['ContactRequest']['name'] . " " . " <" . $this->data['ContactRequest']['email'] . ">";

                $this->Email->send(
                    "Name    : "   . $this->data['ContactRequest']['name']   . "\n" .
                    "Phone   : "   . $this->data['ContactRequest']['phone']  . "\n" .
                    "Email   : "   . $this->data['ContactRequest']['email']  . "\n\n" .
                    "Message\n-----------------------\n" . $this->data['ContactRequest']['message'] . "\n"
                );

                $this->redirect( Configure::read('ContactRequest.url') . '/confirm' );

            } else {
                $this->redirect( Configure::read('ContactRequest.url') );
            }
        }
    }

    function confirm() {
        $this->render( 'confirm' );
    }

    // admin index
    function admin_index() {

        $this->paginate = array(
            'order' => array( 'ContactRequest.created DESC' ),
        );

		$this->ContactRequest->recursive = 1;

        if( !empty( $this->params['form']['q'] ) ) {
			$this->redirect( "/admin/contact_request/index/q:" . $this->params['form']['q'] );
		}

		if( !empty( $this->params['named']['q'] ) ) {
			$this->paginate['conditions'] = array(
				"OR" => array (
                    "ContactRequest.email LIKE" => "%" . $this->params['named']['q'] . "%",
				)
			);
		}

		$this->set( 'contact_requests', $this->paginate( 'ContactRequest' ) );
	}


    // admin export
    function admin_export() {

        $this->ContactRequest->recursive = 1;

		// records
        $records = $this->ContactRequest->find( 'all', array( 'order' => array( 'ContactRequest.created DESC' ) ) );
        $this->set( 'records', $records );

		Configure::write('debug', '0');
		$this->set( 'filename', 'Contact_Request' . date( 'Ymd_H-i' ) . '.csv' );
        $this->render( 'admin_export', 'csv' );
	}


	// admin - delete record
    function admin_delete( $id = null ) {

        // make sure the id has been set before we try to delete
        if( !$id ) {
			$this->Session->setFlash(__('Invalid ID for Record', true), 'default', array( 'class' => 'error' ) );
			$this->redirect( $this->referer() );
		}

		// do the delete
        if ($this->ContactRequest->delete($id)) {
			$this->Session->setFlash(__('Record deleted', true), 'default', array( 'class' => 'success' ) );
			$this->redirect( $this->referer() );
		}
    }
}