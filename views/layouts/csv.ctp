<?php
    $saveas = isset( $filename ) ? $filename : 'Export.csv';

    if( strstr( $_SERVER['HTTP_USER_AGENT'], "MSIE" ) ) {
        header( "Pragma: public" );
        header( "Expires: 0" );
        header( "Cache-Control: must-revalidate, post-check=0, pre-check=0" );
        header( 'Content-Encoding: UTF-8' );
        header( "Content-type: text/csv; charset=UTF-8" );
        header( "Content-Disposition: attachment; filename=\"$saveas\"" );
    } else {
        header( "Expires: Mon, 28 Oct 2008 05:00:00 GMT" );
        header( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
        header( "Cache-Control: no-cache, must-revalidate" );
        header( "Pragma: no-cache" );
        header( 'Content-Encoding: UTF-8' );
        header( "Content-type: text/csv; charset=UTF-8" );
        header( "Content-Disposition: attachment; filename=\"$saveas\"" );
        header( "Content-Description: Generated Report" );
    }

    echo "\xEF\xBB\xBF";
    echo $content_for_layout
?>