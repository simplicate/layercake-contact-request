<h1>Contact Requests</h1>

<? if( sizeof( $contact_requests ) == 0 ): ?>
    <h3>No records found that match your search criteria.</h3>
    <p><?= $html->link( 'Reset Search', array('action' => 'index' ) ); ?></p>
<? else: ?>

    <?= $paginator->options( array( 'url' => $this->passedArgs ) ); ?>

    <?= $html->link( "Export Requests", '/admin/contact_request/export', array( 'class' => 'button', 'escape' => false )  ); ?>

    <table class="list">
        <thead>
            <tr>
                <th><?= $paginator->sort('first_name');?></th>
                <th><?= $paginator->sort('last_name');?></th>
                <th><?= $paginator->sort('email');?></th>
                <th><?= $paginator->sort('created');?></th>
                <th class="t-center">Actions</th>
            </tr>
        </thead>

        <tbody>
            <? foreach( $contact_requests AS $request ): ?>
                <tr class="<?= $cycle->cycle('', 'bg'); ?>">
                    <td><?= $request['ContactRequest']['first_name'];   ?></td>
                    <td><?= $request['ContactRequest']['last_name'];    ?></td>
                    <td><?= $request['ContactRequest']['email'];        ?></td>
                    <td><?= $request['ContactRequest']['created'];      ?></td>
                    <td class="t-center icons">
                        <?= $html->link('<img src="/layer_cake/images/ico-mini-delete.png" alt="Delete" />', array( 'action' => 'delete', $request['ContactRequest']['id']), array( 'escape' => false ), 'Are you sure you want to delete this record?' ); ?>
                    </td>
                </tr>
            <? endforeach; ?>
        </tbody>
    </table>

    <? if( $this->params['paging']['ContactRequest']['pageCount'] > 1 ): ?>
        <!-- Pagination -->
        <div class="pagination bottom box">
            <p class="f-left"><?= $paginator->counter(); ?></p>

            <p class="f-right">
                <?= $paginator->prev('«'); ?>
                <?= $paginator->numbers();		 ?>
                <?= $paginator->next('»'); ?>
            </p>
        </div>
    <? endif; ?>
<? endif; ?>