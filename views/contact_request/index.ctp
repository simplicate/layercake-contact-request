<?php
    $this->set( 'title_for_layout', Configure::read('ContactRequest.title') );
    $this->params['url']['url'] = Configure::read('ContactRequest.url');
?>

<?= $this->element( 'form' ); ?>