<?php
    $this->set( 'title_for_layout', Configure::read('ContactRequest.title') );
    $this->params['url']['url'] = Configure::read('ContactRequest.url');
?>

<p>
	<b>Thank you for contacting us.</b>
</p>

<p>We will do our best to respond to all inquiries within 24 hours.</p>