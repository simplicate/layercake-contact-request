<?
    Router::connect( '/admin/contact_request',             array( 'admin' => true, 'prefix' => 'admin', 'plugin' => 'ContactRequest', 'controller' => 'contact_request', 'action' => 'index' ) );
    Router::connect( '/admin/contact_request/:action/*',   array( 'admin' => true, 'prefix' => 'admin', 'plugin' => 'ContactRequest', 'controller' => 'contact_request' ) );