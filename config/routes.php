<?
    $contact_url = Configure::read('ContactRequest.url');
    Router::connect( "$contact_url",
        array(
			'plugin'		=> 'ContactRequest',
			'controller'	=> 'contact_request',
			'action'		=> 'index'
        )
    );

    Router::connect(
        "$contact_url/:action/*",
        array(
			'plugin'		=> 'ContactRequest',
			'controller'	=> 'contact_request',
        )
    );