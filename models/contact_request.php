<? class ContactRequest extends AppModel {

	var $name = 'ContactRequest';

    // validation in a constructor function so we can translate the messages
    protected function _validationRules() {
        $this->validate = array(
            'name'         => array(
                'rule'    => 'notempty',
                'message' => __('Please enter your name', true )
            ),

            'message'         => array(
                'rule'    => 'notempty',
                'message' => __('Please enter a message', true )
            ),

            'email'         => array(
                'email' => array(
                    'rule'    => 'email',
                    'message' => __('Check you typed your email properly', true )
                ),

                'notempty' => array(
                    'rule'    => 'notempty',
                    'message' => __('Please enter your email address', true )
                ),
            ),
        );
    }
}